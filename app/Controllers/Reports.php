<?php namespace App\Controllers;

use App\Libraries\HondaApi;

class Reports extends BaseController
{
    public function get_date_report($type = null, $search_date = null)
	{
        $honda_api = new HondaApi;		

		if ($type != null && $search_date != null)
		{
			$data = $honda_api->get_date_report($type, $search_date);
		} 
		else 
		{
			$current_week = date('Y') . '-W' . date('W');
			$data =	$honda_api->get_date_report($type, $current_week);
		}
		
		return view('reports_dashboard', $data);
	}
}