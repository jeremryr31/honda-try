<?php namespace App\Controllers;

class Home extends BaseController
{

	public function index($type = null, $date = null)
	{
		$url_inbox_chat = 'https://honda-service-api.herokuapp.com/api/v1/queue/paginate?qAgent=&page=1&qName=&qStatus=1';
		$url_send_msg = 'https://honda-service-api.herokuapp.com/api/v1/message/webhook';
		
		$data = [
			'url_inbox_chat' => $url_inbox_chat,
			'url_send_msg' => $url_send_msg
		];
		
		return view('chatbox', $data);
		
	}

	public function chatbox()
	{
		$url_inbox_chat = 'https://honda-service-api.herokuapp.com/api/v1/queue/paginate?qAgent=&page=1&qName=&qStatus=1';
		$url_send_msg = 'https://honda-service-api.herokuapp.com/api/v1/message/webhook';
		
		$data = [
			'url_inbox_chat' => $url_inbox_chat,
			'url_send_msg' => $url_send_msg
		];
		
		return view('chatbox', $data);
		
	}
	
}
