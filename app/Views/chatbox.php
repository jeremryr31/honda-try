<?php $this->extend('layouts/main'); ?>

<?php $this->section('content'); ?>

  <style>
    .chatbox-honda{
      -webkit-box-shadow: 0px 0px 2px 1px rgba(0,0,0,0.75);
      -moz-box-shadow: 0px 0px 2px 1px rgba(0,0,0,0.75);
      box-shadow: 0px 0px 2px 1px rgba(0,0,0,0.75);
    }

    .user-chat{
      color: #cc0001;
    }
  </style>

  <div class="container">
    <div  class="m-4 p-4 border border-white rounded chatbox-honda">
      <h1 class="text-secondary">Chatbox</h1>
      <hr>
      <div class="row m-4 text-secondary" id="chat-box-messages"></div>
      <hr>
      <div class="row m-4">
        <form class="col-12">
          <div class="row">
            <div class="col-10">
              <input type="text" name="chat_message" class="form-control">
            </div>
            <div class="col-2">
              <input type="submit" value="Send" id="send_chat" class="btn btn-primary" />
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  
  <script>
  var $chat_box_messages = $('#chat-box-messages');
 
  $(() => {
    get_chat_messages();
  })

  function get_chat_messages(){
    $.ajax({
      type: 'GET',
      url: '<?php echo $url_inbox_chat ?>',
      success: data => {
        //console.log(data);
        $.each(data.data.data, (i, msg) => {
          console.log(msg);
          var htmlFeed = '';
          var msg_date = new Date(msg.last_activity.timestamp);
          var hour    = msg_date.getHours();
          var minute  = msg_date.getMinutes();
          var seconds = msg_date.getSeconds();  
          var date_format = msg_date.toDateString() + "<br/>" + hour + ":" + minute + ":" +seconds;
          htmlFeed += "<div class=\"col-12\"><div class=\"row\">";
          htmlFeed += "<div class=\"col-8\"><span><b>" + msg.client + "</b></span><p>" + msg.last_activity.message.text + "</p></div>";
          htmlFeed += "<div class=\"col-4\"><span>" + date_format + "</span></div></div></div>";

          $chat_box_messages.append(htmlFeed);
        })
      }
    });
  }

  $('#send_chat').click(function(e){
    e.preventDefault();
    var chat_message = $('#chat_message').val();
    var date_now = new Date();
    var date_now = date_now.toISOString();
    console.log(date_now);
    var send_data = {
      "newMsg": true,
      "fromCms": true,
      "agent": {
        "_id": "5efedb63db66330017a32920",
        "name": "cbphagent"
      },
      "queue": "5f1e57aa3ce01e0017b40fc8",
      "message": {
        "text": chat_message
      },
      "read": true,
      "type": 1,
      "timestamp": date_now
    }
    $.ajax({
      type: 'POST',
      url: '<?php echo $url_send_msg ?>',
      data: send_data,
      success: data => {
        console.log(data);
        get_chat_messages();
      },
      error: error => {
        console.log(error);
      }
    });
  });

  </script>

<?php $this->endSection(); ?>

   