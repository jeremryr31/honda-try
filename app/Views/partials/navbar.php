
<style>
  #navbar_honda{
    background-color: rgb(222, 24, 46);
  }
</style>

<nav class="navbar navbar-expand-md" id="navbar_honda">
  <a class="navbar-brand text-white font-weight-bold" href="<?= base_url() ?>">Honda</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link text-white" href="<?= base_url() ?>/reports/get_date_report">Dashboard <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link text-white" href="<?= base_url() ?>/chatbox">Chat</a>
      </li>
    </ul>
  </div>
</nav>