<?php $this->extend('layouts/main') ?>

<?php $this->section('content') ?>
  <style>
    .box-report {
      -webkit-box-shadow: 0px 0px 4px 0px rgba(0,0,0,0.75);
      -moz-box-shadow: 0px 0px 4px 0px rgba(0,0,0,0.75);
      box-shadow: 0px 0px 4px 0px rgba(0,0,0,0.75);
    }

    #week, #month, #year {
      max-width: 100px;
    }

    .btn-go {
      background-color: #ff471a;
      color: white;
      border: 1px solid #ff471a;
    }
    
    .mini-title {
      font-size: 0.7em;
      font-weight: bold;
    }

    .count-badge-red{
      background-color: #ff471a;
      border-radius: 5px;
      color: white;
      font-size: 0.8em;
    }

    .count-badge-green {
      background-color: #2eb82e;
      border-radius: 5px;
      color: white;
      font-size: 0.8em;
    }

    .border-gray-color {
      border: 2px solid #bfbfbf;
    }

    .border-gray-color-bottom {
      border-bottom: 2px solid #bfbfbf;
    }

    .text-color-gray {
      color: #737373;
    }

    .transcript-date {
      font-size: 0.9em;
    }
  </style>

  <div class="container p-4">
    <div class="row">
      <div class="col-11 box-report m-3">
        <form action="<?= base_url() ?>" id="calendar_form" method="GET">
          <div class="row">
            <div class="col-12 p-3">
              <b>Get Reports By:</b>
              <span><?= $_SESSION['current_date_search'] ?></span>
            </div>
            <div class="col-sm-4 p-2 px-3">
              <label for="week">Select a week:</label>
              <input type="week" id="week">
              <input onclick="get_report_calendar('week')" class="btn-go" type="submit" value="Go">
            </div>
            <div class="col-sm-4 p-2 px-3">
              <label for="month">Select month:</label>
              <input type="month" id="month">
              <input onclick="get_report_calendar('month')" class="btn-go" type="submit" value="Go">
            </div>
            <div class="col-sm-4 p-2 px-3">
              <label for="year">Select year:</label>
              <input value="<?= date('Y') ?>" type="number" id="year" min="1900" max="2099" step="1"/>
              <input onclick="get_report_calendar('year')" class="btn-go" type="submit" value="Go">
            </div>
          </div>
        </form>
      </div>

      <div class="col-md-5 box-report m-2">
        <div class="row p-3">
          <div class="col-12">
            <b>Week Message Activity:</b>
          </div>
          <div class="col-sm-4 text-center p-3">
            <span class="mini-title">Incoming</span><br>
            <span id="incoming-msg-activity" class="font-weight-bold">Loading...</span><br>
            <span id="incoming-msg-activity-badge" class="count-badge-green p-1 px-2">Loading...</span>
          </div>
          <div class="col-sm-4 text-center p-3">
            <span class="mini-title">Outgoing</span><br>
            <span id="outgoing-msg-activity" class="font-weight-bold">Loading...</span><br>
            <span id="outgoing-msg-activity-badge" class="count-badge-green p-1 px-2">Loading...</span>
          </div>
          <div class="col-sm-4 text-center p-3">
            <span class="mini-title">Total</span><br>
            <span id="total-msg-activity" class="font-weight-bold">Loading...</span><br>
            <span id="total-msg-activity-badge" class="count-badge-green p-1 px-2">Loading...</span>
          </div>
          <div class="col-12">
            <small class="text-muted"><i>Difference from last <?= $_SESSION['current_search_type'] ?> records</i></small>
          </div>
        </div>
      </div>

      <div class="col-md-6 box-report m-2">
        <div class="row p-3">
          <div class="col-12">
            <b>Week User Activity:</b>
          </div>
          <div class="col-sm-4 text-center p-3">
            <span class="mini-title">New</span><br>
            <span id="new-user-activity" class="font-weight-bold">Loading...</span><br>
            <span id="new-user-activity-badge" class="count-badge-green p-1 px-2">Loading...</span>
          </div>
          <div class="col-sm-4 text-center p-3">
            <span class="mini-title">Returning</span><br>
            <span id="returning-user-activity" class="font-weight-bold">Loading...</span><br>
            <span id="returning-user-activity-badge" class="count-badge-green p-1 px-2">Loading...</span>
          </div>
          <div class="col-sm-4 text-center p-3">
            <span class="mini-title">Lifetime</span><br>
            <span id="lifetime-user-activity" class="font-weight-bold">Loading...</span><br>
            <span id="lifetime-user-activity-badge" class="count-badge-green p-1 px-2">Loading...</span>
          </div>
          <div class="col-12">
            <small class="text-muted"><i>Difference from last <?= $_SESSION['current_search_type'] ?> records</i></small>
          </div>
        </div>
      </div>
      
      <div class="col-md-5 box-report m-2 mt-3">
        <div class="row p-3">
          <div class="col-12 mb-3">
            <b>Recent Transcript:</b>
            <span class="count-badge-green p-1 px-2 float-right">Live</span>
          </div>

          <div class="col-12 text-color-gray">
            <div id="recent-transcripts" class="row m-1 border-gray-color border-bottom-0 rounded">
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6 box-report m-2 mt-3">
        <div class="row p-3">
          <div class="col-12">
            <b>Live Chat Messege Rate:</b>
            <span class="count-badge-green p-1 px-2 float-right">Live</span>
          </div>
        </div>
      </div>
    </div>
  </div>

  <script>
    function get_report_calendar(type) {
      let search_date = $("#" + type).val();
      let form_action_value = $("#calendar_form").attr("action"); 
      
      $("#calendar_form").attr("action", form_action_value + "/reports/get_date_report/" + type + "/" + search_date);

    }

    $(() => {
      //get_msg_activity();
      //get_user_activity();
      //get_recent_transcript();
      //get_live_chat_msg_rate();
    })

    let incoming_msg_value = 0;
    let outgoing_msg_value = 0;
    let total_msg_value = 0;

    let new_user_value = 0;
    let returning_user_value = 0;
    let lifetime_user_value = 0;

    const monthNames = ["January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December"
    ];

    function get_msg_activity() {
      let incoming_msg_activity = $("#incoming-msg-activity"); 
      let outgoing_msg_activity = $("#outgoing-msg-activity"); 
      let total_msg_activity = $("#total-msg-activity"); 
      
      $.ajax({
        type: "GET",
        url: '<?= $url_msg_activity ?>',
        success: data => {
          console.log(data);
          update_dom(incoming_msg_activity, data.data.incomingCount);
          update_dom(outgoing_msg_activity, data.data.outgoingCount);
          update_dom(total_msg_activity, data.data.total);
          
          incoming_msg_value = data.data.incomingCount;
          outgoing_msg_value = data.data.outgoingCount;
          total_msg_value = data.data.total;

          get_last_msg_activity();
        },
        error: error => {
          console.log(error);
        }
      })
    }

    function get_last_msg_activity() {
      let incoming_msg_activity_badge = $("#incoming-msg-activity-badge"); 
      let outgoing_msg_activity_badge = $("#outgoing-msg-activity-badge"); 
      let total_msg_activity_badge = $("#total-msg-activity-badge"); 

      $.ajax({
        type: "GET",
        url: '<?= $url_last_msg_activity ?>',
        success: data => {
          console.log(data);
          let incomin_diff = incoming_msg_value - data.data.incomingCount;
          let outgoing_diff = outgoing_msg_value - data.data.outgoingCount;
          let total_diff = total_msg_value - data.data.total;

          update_badge_class(incomin_diff, incoming_msg_activity_badge);
          update_badge_class(outgoing_diff, outgoing_msg_activity_badge);
          update_badge_class(total_diff, total_msg_activity_badge);

          update_dom(incoming_msg_activity_badge, incomin_diff);
          update_dom(outgoing_msg_activity_badge, outgoing_diff);
          update_dom(total_msg_activity_badge, total_diff);
        },
        error: error => {
          console.log(error);
        }
      })
    }

    function get_user_activity() {
      let new_user_activity = $("#new-user-activity"); 
      let returning_user_activity = $("#returning-user-activity"); 
      let lifetime_user_activity = $("#lifetime-user-activity"); 
      
      $.ajax({
        type: "GET",
        url: '<?= $url_user_activity ?>',
        success: data => {
          console.log(data);
          update_dom(new_user_activity, data.data.new);
          update_dom(returning_user_activity, data.data.return);
          update_dom(lifetime_user_activity, data.data.currentLifetime);

          new_user_value = data.data.new;
          returning_user_value = data.data.return;
          lifetime_user_value = data.data.currentLifetime;
          
          get_last_user_activity();
        },
        error: error => {
          console.log(error);
        }
      })
    }

    function get_last_user_activity() {
      let new_user_activity_badge = $("#new-user-activity-badge"); 
      let returning_user_activity_badge = $("#returning-user-activity-badge"); 
      let lifetime_user_activity_badge = $("#lifetime-user-activity-badge"); 

      $.ajax({
        type: "GET",
        url: '<?= $url_last_user_activity ?>',
        success: data => {
          console.log(data);
          let new_user_diff = new_user_value - data.data.new;
          let returning_user_diff = returning_user_value - data.data.return;
          let lifetime_user_diff = lifetime_user_value - data.data.currentLifetime;

          update_badge_class(new_user_diff, new_user_activity_badge);
          update_badge_class(returning_user_diff, returning_user_activity_badge);
          update_badge_class(lifetime_user_diff, lifetime_user_activity_badge);

          update_dom(new_user_activity_badge, new_user_diff);
          update_dom(returning_user_activity_badge, returning_user_diff);
          update_dom(lifetime_user_activity_badge, lifetime_user_diff);
        },
        error: error => {
          console.log(error);
        }
      })
    }

    function update_badge_class(diff, badge_dom) {
      if (diff < 0) {
        badge_dom.removeClass("count-badge-green");
        badge_dom.addClass("count-badge-red");
      }
    }

    function update_dom(dom, append){
      dom.empty();
      dom.append(append);
    }
    
    function get_recent_transcript() {
      let recent_transcripts = $('#recent-transcripts');
      
      $.ajax({
        type: "GET",
        url: "<?= $url_recent_transcript ?>",
        success: data => {
          console.log(data);
          $.each(data.data.ts, (i, transcript) => {
            var htmlFeed = "";
            var user = ""
            var body = transcript.body.pop();
            var msg = "";

            if (transcript.user != null) {
              user = transcript.user.name.first + " " + transcript.user.name.last;
            } else {
              user = "No user";
            }
          
            if (body.message == null || body.message == " " || body.message == undefined) {
              msg = "No message";
            } else {
              msg = body.message.substring(0, 30) + "...";
            }
            
            htmlFeed += "<div class=\"col-12 p-2 px-3 border-gray-color-bottom\"><div class=\"row\">"; 
            htmlFeed += "<div class=\"col-8\"><span class=\"font-weight-bold\">" + user + "</span><br>"
            htmlFeed += "<span>" + msg + "</span></div>";

            var msg_date = new Date(body.timestamp);
            var day    = msg_date.getDate();
            var year    = msg_date.getFullYear();
            var hour    = msg_date.getHours();
            var minute  = msg_date.getMinutes();
            var seconds = msg_date.getSeconds();  

            var ampm = hour >= 12 ? 'pm' : 'am';
            hour = hour % 12;
            hour = hour ? hour : 12;
            minute = minute < 10 ? '0' + minute : minute;
            var time = hour + ":" + minute + ":" +seconds + " " + ampm;

            var date_format = monthNames[msg_date.getMonth()] + " " + day + ", " + year;
            date_format +=  "<br/>" + time;

            htmlFeed += "<div class=\"col-4 transcript-date\"><span>" + date_format + "</span></div></div></div>";

            recent_transcripts.append(htmlFeed);
          })
        },
        error: error => {
          console.log(error);
        }
      })
    }
    
    function get_live_chat_msg_rate() {
      $.ajax({
        type: "GET",
        url: '<?= $url_live_chat_msg_rate ?>',
        success: data => {
          console.log(data);
        },
        error: error => {
          console.log(error);
        }
      })
    }
    
  </script>
  
<?php $this->endSection() ?>