<?php
namespace App\Libraries;

class HondaApi
{
    public function get_date_report($type = null, $search_date = null)
    {
		$iso_dates = $this->get_dates($type, $search_date);
		
		$url_api = $this->get_api_reports($iso_dates['start'], $iso_dates['end'], $iso_dates['last_start'], $iso_dates['last_end']);
		
		$this->set_session_date_search($type, $search_date);
		
		return $url_api;
    } 

	public function get_dates($type, $search_date)
	{
		if ($type == 'week' || $type == null)
		{
			$dates = $this->get_week_dates($search_date);
		}
		else if ($type == 'month')
		{
			$dates = $this->get_month_dates($search_date);
		}
		else if ($type == 'year')
		{
			$dates = $this->get_year_dates($search_date);
		}

		return $dates;
	}

	public function get_week_dates($search_date)
	{
        $year = substr($search_date,0, -4);
        $week_number = substr($search_date,6);

        for ($day=1; $day<=7; $day++)
        {
            $days = date('Y-m-d', strtotime($year.'W'.$week_number.$day));
            
            if ($day == 1)
            {
                $first_day = $days;
            }
            else if ($day == 7)
            {
                $last_day = $days . ' 23:59:59.999';
            }
        }
        
        $start_date_iso = date('c', strtotime($first_day));
        $end_date_iso = date('c', strtotime($last_day));

        $last_year = date('Y', strtotime('-1 week ' . $start_date_iso));
        $last_week_number = date('W', strtotime('-1 week ' . $start_date_iso));

        for ($day=1; $day<=7; $day++)
        {
            $days = date('Y-m-d', strtotime($last_year . 'W' . $last_week_number . $day));
            
            if ($day == 1)
            {
                $last_first_day = $days;
            }
            else if ($day == 7)
            {
                $last_last_day = $days . ' 23:59:59.999';
            }
        }

        $last_start_date_iso = date('c', strtotime($last_first_day));
        $last_end_date_iso = date('c', strtotime($last_last_day));
        
        $dates = $this->get_array_dates($start_date_iso, $end_date_iso, $last_start_date_iso, $last_end_date_iso);

        return $dates;
    }

    public function get_month_dates($search_date)
    {
        $first_day =  date('Y-m-01', strtotime($search_date)); 
        $last_day = date('Y-m-t', strtotime($search_date));
        
        $start_date_iso = date('c', strtotime($first_day));
		$end_date_iso = date('c', strtotime($last_day . ' 23:59:59.999'));

		$last_first_day = date('Y-m-01', strtotime('-1 month' . $start_date_iso));
		$last_last_day = date('Y-m-t', strtotime('-1 month' . $end_date_iso));
		
		$last_start_date_iso = date('c', strtotime($last_first_day));
		$last_end_date_iso = date('c', strtotime($last_last_day . ' 23:59:59.999'));
		
		$dates = $this->get_array_dates($start_date_iso, $end_date_iso, $last_start_date_iso, $last_end_date_iso);
		
		return $dates;
	}
	
	public function get_year_dates($search_date)
	{
		$first_day = date('Y-m-d', strtotime($search_date . '-01-01')); 
		$last_day = date('Y-m-d', strtotime($search_date . '-12-31'));
		
        $start_date_iso = date('c', strtotime($first_day));
		$end_date_iso = date('c', strtotime($last_day . ' 23:59:59.999'));

		$last_first_day = date('Y-01-01', strtotime('-1 years' . $start_date_iso));
		$last_last_day = date('Y-12-31', strtotime('-1 years' . $end_date_iso));
		
		$last_start_date_iso = date('c', strtotime($last_first_day));
		$last_end_date_iso = date('c', strtotime($last_last_day . ' 23:59:59.999'));
		
		$dates = $this->get_array_dates($start_date_iso, $end_date_iso, $last_start_date_iso, $last_end_date_iso);
		
		return $dates;
	}

	public function get_array_dates($start, $end, $last_start, $last_end)
    {
        $dates = [
			'start' => $start,
			'end' => $end,
			'last_start' => $last_start,
			'last_end' => $last_end
        ];
        
        return $dates;
    }
	
	public function get_api_reports($start, $end, $last_start, $last_end)
	{
		$honda_api = new \Config\HondaApi();		

		$honda_url = $honda_api->honda_url;
		
		$url_params = 'start=' . $start . '&end=' . $end;

		$url_params_last = 'start=' . $last_start . '&end=' . $last_end;

		$url_msg_activity = $honda_url . 'message-activity?' . $url_params;

		$url_user_activity = $honda_url . 'user-activity?' . $url_params;

		$url_last_msg_activity = $honda_url . 'message-activity?' . $url_params_last;

		$url_last_user_activity = $honda_url . 'user-activity?' . $url_params_last;

		$url_recent_transcript = $honda_url . 'recent-transcripts';

		$url_live_chat_msg_rate = $honda_url . 'top-exit-messages?field';

		$url_api = [
			'url_msg_activity' => $url_msg_activity,
			'url_user_activity' => $url_user_activity,
			'url_last_msg_activity' => $url_last_msg_activity,
			'url_last_user_activity' => $url_last_user_activity,
			'url_recent_transcript' => $url_recent_transcript,
			'url_live_chat_msg_rate' => $url_live_chat_msg_rate
		];

		return $url_api;
	}

	public function set_session_date_search($type, $search_date)
	{
		$session = session();

		if ($type == null)
		{
			$type = "week";
		}
		
		$session->set('current_search_type', $type);

		if ($type == 'month')
		{
			$session->set('current_date_search', date('Y F', strtotime($search_date)));
		}
		else
		{
			$session->set('current_date_search', $search_date);
		}
	}
}


