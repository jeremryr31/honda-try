<?php
namespace Config;

use CodeIgniter\Config\BaseConfig;

class HondaApi extends BaseConfig
{
  public $honda_url = 'https://honda-service-api.herokuapp.com/api/v1/dashboard/';
}